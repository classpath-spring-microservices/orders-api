package com.classpath.ordersapi.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class AppHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        //call the db execute a query
        //call external service and check if all is fine
        //call jms and check if the dependenies are fine
        return Health.up().withDetail("Service-A", "Servic A is up").build();
    }
}